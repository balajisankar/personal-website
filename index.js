var http = require('http');
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var fs = require('fs');

var app = express();

var maxSize = 100000;

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(multer({ dest: './uploads/',
	rename: function(fieldname, filename) {
		return filename+Date.now();
	},
	onFileUploadStart: function(file, req, res){
		console.log(file.fieldname + 'is starting uploading');
	},
	onFileSizeLimit: function(file) {
		console.log("Upload failed for " + file.fieldname);
		fs.unlink('./' + file.path);
	},
	limits: {
		fieldNameSize: 100,
		files: 1,
		fileSize: 100000
	}
}));


app.set('views', './public/views');
app.set('view engine', 'jade');

console.log('Starting server');

var server = http.createServer(app);


app.use('/', require(path.join(__dirname, 'route.js')));


server.listen(8080, function() {
	var host = server.address().address;
	var port = server.address().port;

	console.log('Server listening at http://%s:%s', host, port);
});