var express = require('express');
var path = require('path');
var settings = require('./settings.json');
var fs = require('fs');

var router = express.Router();



router.use(function logRequests(req, res, next) {
	console.log(req.method + ' request at ' + req.url);
	next();
});

router.get('/', function(req, res){
	res.render('home');
});

router.get('/Home', function(req, res){
	res.render('home');
});

router.get('/Resume', function(req, res) {
	res.render('resume');
});

router.get('/getResume', function(req, res) {
	res.sendFile(path.join(__dirname, 'public', 'assets', 'Resume_small.pdf'));
});

router.get('/Projects', function(req, res) {
	res.render('projects');
})

router.get('/Testing', function(req, res) {
	res.render('testing');
})

router.post('/SubmitTest', function(req, res) {
	if (req.body.password == settings.adminpassword) {
		//perform tests
	}
	else {
		fs.unlink('./' + req.files.cppfiles.path);
	}
	res.redirect('/Testing');
})
module.exports = router;